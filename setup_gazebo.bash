#!/bin/bash
#
# Setup environment to make PX4 visible to Gazebo.



# setup Gazebo env and update package path

# Point to plugins in the folder where this script is located
export GAZEBO_PLUGIN_PATH=$GAZEBO_PLUGIN_PATH:~/gazebo_botsy/plugin

# Point to models in the folder where this script is located
export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:~/gazebo_botsy/models

# Point to plugins in the folder where this script is located
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/gazebo_botsy/plugin

echo -e "GAZEBO_PLUGIN_PATH $GAZEBO_PLUGIN_PATH"
echo -e "GAZEBO_MODEL_PATH $GAZEBO_MODEL_PATH"
echo -e "LD_LIBRARY_PATH $LD_LIBRARY_PATH"
