# Master mokeys Gazebo WS


This work space is used to orginize models for simulation durion our master thesis

Not all of the models or plugins are our own origional work.

## Plugins and models from:

https://github.com/ros-simulation/gazebo_ros_pkgs

https://github.com/PX4/PX4-SITL_gazebo


## Models we have modified:


### Drones:

botsey (iris drone derivative)

botsey_sens (botsey drone derivative with imu and zed mini)

### Sensors:

zed_mini (depth camera and imu derivative)

imu (imu sensor model)
