<?xml version="1.0" ?>
<sdf version="1.5">
  <model name="zed_mini">
    <pose>0 0 0.035 0 0 0</pose>
    <link name="zed_mini_link">
      <inertial>
        <pose>0.01 0.025 0.025 0 0 0</pose>
        <mass>0.01</mass>
        <inertia>
          <ixx>4.15e-6</ixx>
          <ixy>0</ixy>
          <ixz>0</ixz>
          <iyy>2.407e-6</iyy>
          <iyz>0</iyz>
          <izz>2.407e-6</izz>
        </inertia>
      </inertial>
      <visual name="visual">
        <pose>0 0 0 0 0 0</pose>
        <geometry>
          <mesh>
            <uri>model://realsense_camera/meshes/realsense.dae</uri>
          </mesh>
        </geometry>
      
      </visual>
      <!--https://github.com/ros-simulation/gazebo_ros_pkgs/wiki/ROS-2-Migration:-Camera-->
      <!--https://github.com/mlherd/ros2_turtlebot3_waffle_intel_realsense/blob/master/turtlebot3_waffle/model.sdf-->
      
      <!-- Depth Camera -->
      <sensor name="zed_mini_depth" type="depth">  
        <always_on>true</always_on>
        <update_rate>10</update_rate>
        <camera name="depth">
          <horizontal_fov>1.57</horizontal_fov>
          <image>
            <format>R8G8B8</format>
            <width>128</width>
            <height>72</height>
          </image>
          <clip>
            <near>0.1</near>
            <far>50</far>
          </clip>
          <noise>
            <type>gaussian</type>
            <mean>0.0</mean>
            <stddev>0.05</stddev>
          </noise>
          <distortion>
              <k1>0.0</k1>
              <k2>0.0</k2>
              <k3>0.0</k3>
              <p1>0.0</p1>
              <p2>0.0</p2>
              <center>0.5 0.5</center>
            </distortion>
        </camera>
        <plugin filename="libgazebo_ros_camera.so" name="RosCameraPlugin_Depth">
          <frame_name>camera</frame_name>
        </plugin>
      </sensor>

      <!-- RGB Camera -->
      <sensor name="zed_mini_rgb" type="camera">  
        <always_on>true</always_on>
        <update_rate>30</update_rate>
        <camera name="rgb">
          <horizontal_fov>1.57</horizontal_fov>
          <image>
            <format>R8G8B8</format>
            <width>600</width>
            <height>400</height>
          </image>
          <clip>
            <near>0.1</near>
            <far>30</far>
          </clip>
          <noise>
            <type>gaussian</type>
            <mean>0.0</mean>
            <stddev>0.05</stddev>
          </noise>
          <distortion>
              <k1>0.0</k1>
              <k2>0.0</k2>
              <k3>0.0</k3>
              <p1>0.0</p1>
              <p2>0.0</p2>
              <center>0.5 0.5</center>
            </distortion>
        </camera>
        <!--plugin filename="libgazebo_ros_camera.so" name="RosCameraPlugin_Camera">
          <frame_name>zed_mini_camera_frame</frame_name>
        </plugin-->
        <plugin filename="libgazebo_gst_camera_plugin.so" name="GstCameraPlugin">
            <robotNamespace></robotNamespace>
            <udpHost>127.0.0.1</udpHost>
            <udpPort>5600</udpPort>
        </plugin>
      </sensor>

      <!-- IMU Sensor-->
      <sensor name="imu" type="imu">
        <always_on>true</always_on>
        <update_rate>100</update_rate>
        <imu>
          <angular_velocity>
            <x>
              <noise type="gaussian">
                <mean>0.0</mean>
                <stddev>2e-4</stddev>
              </noise>
            </x>
            <y>
              <noise type="gaussian">
                <mean>0.0</mean>
                <stddev>2e-4</stddev>
              </noise>
            </y>
            <z>
              <noise type="gaussian">
                <mean>0.0</mean>
                <stddev>2e-4</stddev>
              </noise>
            </z>
          </angular_velocity>
          <linear_acceleration>
            <x>
              <noise type="gaussian">
                <mean>0.0</mean>
                <stddev>1.7e-2</stddev>
              </noise>
            </x>
            <y>
              <noise type="gaussian">
                <mean>0.0</mean>
                <stddev>1.7e-2</stddev>
              </noise>
            </y>
            <z>
              <noise type="gaussian">
                <mean>0.0</mean>
                <stddev>1.7e-2</stddev>
              </noise>
            </z>
          </linear_acceleration>
        </imu>
        <plugin filename="libgazebo_ros_imu_sensor.so" name="RosImuPlugin_zed_mini">
          <ros>
            <argument>~/out:=sensor/imu_aux</argument>
            <frame_name>zed_mini_imu_frame</frame_name>  
          </ros>
        </plugin>
      </sensor>
      
    </link>

    <!-- P3D publishes ground truth data of object -->
    <plugin name="gazebo_ros_p3d_zed_mini" filename="libgazebo_ros_p3d.so">
      <ros>
        <argument>odom:=botsy/odom/imu_aux_sim</argument>
      </ros>
      <body_name>zed_mini_link</body_name>
      <frame_name>map</frame_name>
      <update_rate>10</update_rate>
      <xyz_offset>0.0 0.0 0.0</xyz_offset>
      <rpy_offset>0.0 0.0 0.0</rpy_offset>
      <gaussian_noise>0.01</gaussian_noise>
    </plugin>
  </model>
</sdf>
